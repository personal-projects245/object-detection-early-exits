import argparse
import numpy as np
import cv2
import time
from PIL import Image
import datetime
from pycoral.adapters import common
from pycoral.adapters import detect
from pycoral.utils.edgetpu import make_interpreter
from pycoral.utils.dataset import read_label_file
import os
import threading

#This file allows the object detection model to be run asynchronous resulting in a faster 
#object detection but increasing power output. With one thread for object detection and
#one thread for processing the video. Multithreading with Python was a chore and is not
#the most suitable language for this I discovered.

# Global flag for early exit condition
object_frame = -1
object_timestamp = ""
object_found_event = threading.Event()
object_lock = threading.Lock()
main_thread_event = threading.Event()

def ReadLabelFile(file_path):
    with open(file_path, 'r') as f:
        lines = f.readlines()
    ret = {}
    for line in lines:
        pair = line.strip().split(maxsplit=1)
        ret[int(pair[0])] = pair[1].strip()
    return ret

def object_detection_thread(args, labels, interpreter, input_width, input_height, orig_camera_width, orig_camera_height):
    global object_frame, object_timestamp, object_lock, object_found_event, main_thread_event

    cap = cv2.VideoCapture(args.video)
    output_folder = "output_images/"

    while not object_found_event.is_set():

        t1 = time.perf_counter()

        ret, color_image = cap.read()
        if not ret:
            break

        # Resize the frame to match the model's input dimensions
        resized_image = cv2.resize(color_image, (input_width, input_height))

        # Preprocess the image.
        prepimg = resized_image[:, :, ::-1].copy()
        prepimg = Image.fromarray(prepimg)
        prepimg = prepimg.resize((input_width, input_height), Image.ANTIALIAS)

        # Run inference.
        input_tensor = interpreter.tensor(interpreter.get_input_details()[0]['index'])
        common.set_input(interpreter, prepimg)
        interpreter.invoke()
        objs = detect.get_objects(interpreter, args.conf_thresh)

        # Display the result.
        for obj in objs:
            # Draw square around the detected object
            box = obj.bbox
            box_left = int(box[0] * (orig_camera_width / input_width))
            box_top = int(box[1] * (orig_camera_height / input_height))
            box_right = int(box[2] * (orig_camera_width / input_width))
            box_bottom = int(box[3] * (orig_camera_height / input_height))
            cv2.rectangle(color_image, (box_left, box_top), (box_right, box_bottom), (0, 255, 0), 2)

            # Draw object label and accuracy percentage
            label_text = f"{labels[obj.id]} ({int(obj.score * 100)}%)"
            cv2.putText(color_image, label_text, (box_left, box_top - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 255, 0), 2)

            # Save the frame as an image if the specific object is detected
            if labels[obj.id] == args.search_object:
                with object_lock:
                    object_found_event.set()
                    object_frame = int(cap.get(cv2.CAP_PROP_POS_FRAMES))
                    object_timestamp = time.strftime("%H:%M:%S", time.gmtime(object_frame / args.fps))
                    print_data()

                timestamp_str = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
                image_filename = f"{output_folder}object_found_{timestamp_str}.jpg"
                cv2.imwrite(image_filename, color_image)
                main_thread_event.set()  # Notify the main thread that object is found
                break

        # Display the original video frame
        cv2.namedWindow('Video', cv2.WINDOW_AUTOSIZE)
        cv2.imshow('Video', color_image)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    cap.release()

def process_video(args, labels, interpreter, input_width, input_height, orig_camera_width, orig_camera_height):
    global object_frame, object_timestamp, main_thread_event

    # Open video file
    cap = cv2.VideoCapture(args.video)

    # Check if video file was successfully opened
    if not cap.isOpened():
        print("Error opening video file:", args.video)
        return

    while not object_found_event.is_set():
        ret, color_image = cap.read()
        if not ret:
            break

        # Resize the frame to match the model's input dimensions
        resized_image = cv2.resize(color_image, (input_width, input_height))

        # Preprocess the image.
        prepimg = resized_image[:, :, ::-1].copy()
        prepimg = Image.fromarray(prepimg)
        prepimg = prepimg.resize((input_width, input_height), Image.ANTIALIAS)

        # Run inference.
        input_tensor = interpreter.tensor(interpreter.get_input_details()[0]['index'])
        common.set_input(interpreter, prepimg)
        interpreter.invoke()
        objs = detect.get_objects(interpreter, args.conf_thresh)

        # Display the result.
        for obj in objs:
            # Draw square around the detected object
            box = obj.bbox
            box_left = int(box[0] * (orig_camera_width / input_width))
            box_top = int(box[1] * (orig_camera_height / input_height))
            box_right = int(box[2] * (orig_camera_width / input_width))
            box_bottom = int(box[3] * (orig_camera_height / input_height))
            cv2.rectangle(color_image, (box_left, box_top), (box_right, box_bottom), (0, 255, 0), 2)

            # Draw object label and accuracy percentage
            label_text = f"{labels[obj.id]} ({int(obj.score * 100)}%)"
            cv2.putText(color_image, label_text, (box_left, box_top - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 255, 0), 2)

        # Display the original video frame
        cv2.namedWindow('Video', cv2.WINDOW_AUTOSIZE)
        cv2.imshow('Video', color_image)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    cap.release()


def print_data():
        # Print object detection details
    if object_found_event.is_set():
            print("Object found at frame:", object_frame)
            print("Timestamp (HH:MM:SS):", object_timestamp)
    else:
        print("Object not found in the video.")

def main():
    global object_frame, object_timestamp, object_found_event, object_lock, main_thread_event

    parser = argparse.ArgumentParser()
    parser.add_argument("--model", default="mobilenet_ssd_v2_coco_quant_postprocess_edgetpu.tflite", help="Path of the detection model.")
    parser.add_argument("--label", default="coco_labels.txt", help="Path of the labels file.")
    parser.add_argument("--video", default="./media/mp4_videos/fruit-and-vegetable-detection.mp4", help="Path of the input video file.")
    parser.add_argument("--fps", type=int, default=30, help="Frames per second of the input video.")
    parser.add_argument("--search_object", default="broccoli", help="Object to search for with early exit.")
    parser.add_argument("--conf_thresh", type=float, default=0.5, help="Confidence threshold for early exit.")
    args = parser.parse_args()

    # Initialize the interpreter.
    labels = read_label_file(args.label) if args.label else None
    interpreter = make_interpreter(args.model, "usb")
    interpreter.allocate_tensors()

    # New dimensions for resizing
    input_width = 300
    input_height = 300

    # Get video properties
    cap = cv2.VideoCapture(args.video)
    orig_camera_width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    orig_camera_height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    cap.release()

    # Start the object detection thread
    t = threading.Thread(target=object_detection_thread, args=(args, labels, interpreter, input_width, input_height, orig_camera_width, orig_camera_height))
    t.start()

    # Process the video frames asynchronously
    process_video(args, labels, interpreter, input_width, input_height, orig_camera_width, orig_camera_height)

    # Wait for the object detection thread to finish before continuing
    t.join(timeout=15)

    # Check if the object detection thread is still alive after the wait
    if t.is_alive():
        print("Timeout: Object detection thread took too long to complete.")
        object_found_event.set()  # Set the event to stop the object detection thread gracefully
        t.join()  # Wait for the thread to finish after setting the event


    cv2.destroyAllWindows()

if __name__ == '__main__':
    main()
