import argparse
import numpy as np
import cv2
import time
from PIL import Image
from pycoral.adapters import common
from pycoral.adapters import detect
from pycoral.utils.edgetpu import make_interpreter
from pycoral.utils.dataset import read_label_file

#Synchronous use of the objecct detection model. 

# Global flag for early exit condition
object_found = False
object_frame = -1
object_timestamp = ""

def ReadLabelFile(file_path):
    with open(file_path, 'r') as f:
        lines = f.readlines()
    ret = {}
    for line in lines:
        pair = line.strip().split(maxsplit=1)
        ret[int(pair[0])] = pair[1].strip()
    return ret

def main():
    global object_found  # Declare object_found as global

    parser = argparse.ArgumentParser()
    parser.add_argument("--model", default="mobilenet_ssd_v2_coco_quant_postprocess_edgetpu.tflite", help="Path of the detection model.")
    parser.add_argument("--label", default="coco_labels.txt", help="Path of the labels file.")
    parser.add_argument("--video", default="./media/mp4_videos/fruit-and-vegetable-detection.mp4", help="Path of the input video file.")
    parser.add_argument("--fps", type=int, default=30, help="Frames per second of the input video.")
    parser.add_argument("--search_object", default="broccoli", help="Object to search for with early exit.")
    parser.add_argument("--confidence_threshold", type=float, default=0.5, help="Confidence threshold for early exit.")
    args = parser.parse_args()

    # Initialize the interpreter.
    labels = read_label_file(args.label) if args.label else None
    interpreter = make_interpreter(args.model, "usb")
    interpreter.allocate_tensors()

    # New dimensions for resizing
    input_width = 300
    input_height = 300

    # Open video file
    cap = cv2.VideoCapture(args.video)

    # Check if video file was successfully opened
    if not cap.isOpened():
        print("Error opening video file:", args.video)
        return

    # Get video properties
    orig_camera_width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    orig_camera_height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

    # Main loop for object detection
    while not object_found:
        t1 = time.perf_counter()

        ret, color_image = cap.read()
        if not ret:
            break

        # Resize the frame to match the model's input dimensions
        resized_image = cv2.resize(color_image, (input_width, input_height))

        # Preprocess the image.
        prepimg = resized_image[:, :, ::-1].copy()
        prepimg = Image.fromarray(prepimg)
        prepimg = prepimg.resize((input_width, input_height), Image.ANTIALIAS)

        # Run inference.
        input_tensor = interpreter.tensor(interpreter.get_input_details()[0]['index'])
        common.set_input(interpreter, prepimg)
        interpreter.invoke()
        objs = detect.get_objects(interpreter, args.confidence_threshold)

        # Display the result.
        for obj in objs:
            # Draw square around the detected object
            box = obj.bbox
            box_left = int(box[0] * (orig_camera_width / input_width))
            box_top = int(box[1] * (orig_camera_height / input_height))
            box_right = int(box[2] * (orig_camera_width / input_width))
            box_bottom = int(box[3] * (orig_camera_height / input_height))
            cv2.rectangle(color_image, (box_left, box_top), (box_right, box_bottom), (0, 255, 0), 2)

            # Draw object label and accuracy percentage
            label_text = f"{labels[obj.id]} ({int(obj.score * 100)}%)"
            cv2.putText(color_image, label_text, (box_left, box_top - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 255, 0), 2)

        # Display the original video frame
        cv2.namedWindow('Video', cv2.WINDOW_AUTOSIZE)
        cv2.imshow('Video', color_image)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

        # FPS calculation
        t2 = time.perf_counter()
        elapsedTime = t2 - t1
        fps = 1 / elapsedTime

    # Release video capture and close windows
    cap.release()
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main()
