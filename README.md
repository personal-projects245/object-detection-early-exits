# TPU-MobilenetSSDv2
## Environment
1. LaptopPC (Ubuntu 22.04)
2. Edge TPU Accelerator (Supports multi-TPU)
3. Web camera

## Libraries
1. Pycoral
2. PIL
3. CV2
4. argparse
5. platform
6. numpy as np
7. cv2
8. time
9. multiprocessing
10. tensorflow 2.13.0

# Usage
**MobileNet-SSD-TPU-EE-async.py -> mp4 video and inference are asynchronous (The frame is slightly off.)**  
**MobileNet-SSD-TPU-EE-sync.py -> mp4 video and inference are synchronous (The frame does not shift greatly.)**

# Folders
- colaboratory - Files for transfer-learning models and training new.
- media - mp4 videos
- old_code - Older code for web camera usage with EdgeTPU
- output_images - Output folder for early exit object


# Flags when running
```bash
usage: MobileNet-SSD-TPU-async.py [-h] [--model MODEL] [--label LABEL]
                                  [--video VIDEO] 
                                  [--fps FPS]
                                  [--search_object OBJECT]
                                  [--conf_thresh PERCENT ]

optional arguments:
  -h, --help              show this help message and exit
  --model MODEL           Path of the detection model.
  --label LABEL           Path of the labels file.
  --video VIDEO           Path to mp4-video
  --fps FPS               Number of fps, alters speed
  --search_object OBJECT  Which object to early exit on
  --conf_thresh PERCENT   Minimum perncentage accuracy for early exit
```

# Reference
- Get started with the USB Accelerator https://coral.withgoogle.com/tutorials/accelerator
- Models https://coral.withgoogle.com/models/
- Edge TPU Model Compiler https://coral.withgoogle.com/web-compiler/
- Edge TPU Benchmark https://coral.withgoogle.com/tutorials/edgetpu-faq/
