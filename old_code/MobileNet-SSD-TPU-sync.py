import argparse
import platform
import numpy as np
import cv2
import time
from PIL import Image
from pycoral.adapters import common
from pycoral.adapters import detect
from pycoral.utils.edgetpu import make_interpreter
from pycoral.utils.dataset import read_label_file


# This file Runs the edgeTPU with the object detection model synchronous with
# a mp4 video file. Requiring less process power but allows less FPS for the detection model

def ReadLabelFile(file_path):
    with open(file_path, 'r') as f:
        lines = f.readlines()
    ret = {}
    for line in lines:
        pair = line.strip().split(maxsplit=1)
        ret[int(pair[0])] = pair[1].strip()
    return ret


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--model", default="mobilenet_ssd_v2_voc_quant_postprocess_edgetpu.tflite", help="Path of the detection model.")
    parser.add_argument("--label", default="voc_labels.txt", help="Path of the labels file.")
    parser.add_argument("--video", default="./media/mp4_videos/fruit-and-vegetable-detection.mp4", help="Path of the input video file.")
    args = parser.parse_args()

    fps = ""
    detectfps = ""
    framecount = 0
    detectframecount = 0
    time1 = 0
    time2 = 0
    box_color = (255, 128, 0)
    box_thickness = 1
    label_background_color = (125, 175, 75)
    label_text_color = (255, 255, 255)
    percentage = 0.0

    cap = cv2.VideoCapture(args.video)

    # Check if video file successfully opened
    if not cap.isOpened():
        print("Error opening video file:", args.video)
        return

    # Get video properties
    orig_camera_width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    orig_camera_height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    vidfps = cap.get(cv2.CAP_PROP_FPS)

    # Initialize the interpreter.
    labels = read_label_file(args.label) if args.label else None
    interpreter = make_interpreter(args.model, "usb")
    interpreter.allocate_tensors()

    # New dimensions for resizing
    input_width = 300
    input_height = 300

    while True:
        t1 = time.perf_counter()

        ret, color_image = cap.read()
        if not ret:
            break

        # Resize the frame to match the model's input dimensions
        resized_image = cv2.resize(color_image, (input_width, input_height))

        # Preprocess the image.
        prepimg = resized_image[:, :, ::-1].copy()
        prepimg = Image.fromarray(prepimg)
        input_details = interpreter.get_input_details()
        input_tensor_shape = input_details[0]['shape']
        input_width, input_height = input_tensor_shape[1], input_tensor_shape[2]
        prepimg = prepimg.resize((input_width, input_height), Image.ANTIALIAS)

        # Run inference.
        tinf = time.perf_counter()
        input_details = interpreter.get_input_details()
        input_tensor = interpreter.tensor(input_details[0]['index'])
        common.set_input(interpreter, prepimg)
        interpreter.invoke()
        objs = detect.get_objects(interpreter, 0.5)

        print(time.perf_counter() - tinf, "sec")

        # Display the result.
        if objs:
            detectframecount += 1
            for obj in objs:
                box = obj.bbox
                box_left = int(box[0] * (orig_camera_width / input_width))
                box_top = int(box[1] * (orig_camera_height / input_height))
                box_right = int(box[2] * (orig_camera_width / input_width))
                box_bottom = int(box[3] * (orig_camera_height / input_height))
                cv2.rectangle(color_image, (box_left, box_top), (box_right, box_bottom), box_color, box_thickness)

                percentage = int(obj.score * 100)
                label_text = labels[obj.id] + " (" + str(percentage) + "%)"

                label_size = cv2.getTextSize(label_text, cv2.FONT_HERSHEY_SIMPLEX, 0.5, 1)[0]
                label_left = box_left
                label_top = box_top - label_size[1]
                if label_top < 1:
                    label_top = 1
                label_right = label_left + label_size[0]
                label_bottom = label_top + label_size[1]
                cv2.rectangle(color_image, (label_left - 1, label_top - 1), (label_right + 1, label_bottom + 1), label_background_color, -1)
                cv2.putText(color_image, label_text, (label_left, label_bottom), cv2.FONT_HERSHEY_SIMPLEX, 0.5, label_text_color, 1)

        cv2.putText(color_image, fps, (orig_camera_width - 170, 15), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (38, 0, 255), 1, cv2.LINE_AA)
        cv2.putText(color_image, detectfps, (orig_camera_width - 170, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (38, 0, 255), 1, cv2.LINE_AA)

        cv2.namedWindow('Video', cv2.WINDOW_AUTOSIZE)
        cv2.imshow('Video', color_image)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

        # FPS calculation
        framecount += 1
        if framecount >= 15:
            fps = "(Playback) {:.1f} FPS".format(time1 / 15)
            detectfps = "(Detection) {:.1f} FPS".format(detectframecount / time2)
            framecount = 0
            detectframecount = 0
            time1 = 0
            time2 = 0
        t2 = time.perf_counter()
        elapsedTime = t2 - t1
        time1 += 1 / elapsedTime
        time2 += elapsedTime


if __name__ == '__main__':
    main()
